package com.bddca.UIStore;

import org.openqa.selenium.By;

public class LandingPageUI {
	public static final By proficon = By.className("user-profile-icon");
	public static final By login = By.cssSelector("[class='login-link authentication_popup']") ;
	public static final By profile = By.cssSelector("[href='/profile']");
	public static final By loginemailbox = By.cssSelector("[placeholder='Email Address']");
	public static final By loginpassbox = By.cssSelector("[placeholder='Password']");
	public static final By signupemailbox = By.xpath("//*[@id='spree_user_email']");
	public static final By signuppassbox = By.xpath("//*[@name='spree_user[password]']");
	public static final By signup = By.cssSelector("[class='signup-link authentication_popup']") ;
	public static final By visitus = By.cssSelector("[href='/furniture-stores?src=g_footer']");
	public static final By contactus = By.cssSelector("[href='/help/contact-us?src=g_footer']");
	public static final By signuperr = By.cssSelector("label[class='error']");
}
