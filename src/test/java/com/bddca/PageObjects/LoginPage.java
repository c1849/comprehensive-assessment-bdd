package com.bddca.PageObjects;

import org.openqa.selenium.WebDriver;

import com.bddca.ReusableComponents.ReusableMethods;
import com.bddca.UIStore.LoginPageUI;

public class LoginPage {
	public static boolean error(WebDriver driver) {
		if(ReusableMethods.getElement(LoginPageUI.errormsg, driver))
			return true;
		return false;
	}
}
