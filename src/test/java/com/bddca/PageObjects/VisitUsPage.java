package com.bddca.PageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class VisitUsPage {
	public static WebElement stores(WebDriver driver,int index) {
		WebElement store = null;
		try {
			List<WebElement> stores = driver.findElements(By.cssSelector("div[class='_3oCTi']  > a"));
			store = stores.get(index);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return store;
	}
	
	public static boolean clickStore(WebDriver driver) {
		WebElement store = stores(driver, 0);
		if(store != null) {
			store.click();
			return true;
		}
		return false;
	}
}
