package com.bddca.PageObjects;

import org.openqa.selenium.WebDriver;

import com.bddca.ReusableComponents.ReusableMethods;
import com.bddca.UIStore.StorePageUI;

public class StorePage {
	public static boolean getAddress(WebDriver driver) {
		if (ReusableMethods.getElement(StorePageUI.address, driver))
			return true;
		return false;
	}
}
