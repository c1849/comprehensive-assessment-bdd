package com.bddca.PageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bddca.ReusableComponents.ReusableMethods;
import com.bddca.UIStore.LandingPageUI;

public class LandingPage {
	public static boolean clickLogIn(WebDriver driver) {
		if (ReusableMethods.click(LandingPageUI.proficon, driver))
			if (ReusableMethods.click(LandingPageUI.login, driver))
				return true;
		return false;
	}

	public static boolean profile(WebDriver driver) {
		if (ReusableMethods.getElement(LandingPageUI.profile, driver))
			return true;
		return false;
	}

	public static boolean enterloginDetails(WebDriver driver, String email, String password) {
		if (ReusableMethods.sendKeys(LandingPageUI.loginemailbox, email, driver))
			if (ReusableMethods.sendKeys(LandingPageUI.loginpassbox, password + Keys.ENTER, driver))
				return true;
		return false;
	}

	public static boolean entersignupDetails(WebDriver driver, String email, String password) {
		if (ReusableMethods.sendKeys(LandingPageUI.signupemailbox, email + Keys.ENTER + password + Keys.ENTER, driver))
//			if (ReusableMethods.sendKeys(LandingPageUI.signuppassbox, password + Keys.ENTER, driver))
			return true;
		return false;
	}

	public static boolean signupError(WebDriver driver) {
		try {
			WebElement error = driver.findElement(LandingPageUI.signuperr);
			if (error.isDisplayed())
				return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static boolean clickSignUp(WebDriver driver) {
		if (ReusableMethods.click(LandingPageUI.proficon, driver))
			if (ReusableMethods.click(LandingPageUI.signup, driver))
				return true;
		return false;
	}

	public static boolean clickVisitUs(WebDriver driver) {
		if (ReusableMethods.click(LandingPageUI.visitus, driver))
			return true;
		return false;
	}

	public static boolean clickContactUs(WebDriver driver) {
		if (ReusableMethods.click(LandingPageUI.contactus, driver))
			return true;
		return false;
	}

}
