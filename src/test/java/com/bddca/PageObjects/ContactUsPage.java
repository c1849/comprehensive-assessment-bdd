package com.bddca.PageObjects;

import org.openqa.selenium.WebDriver;

import com.bddca.ReusableComponents.ReusableMethods;
import com.bddca.UIStore.ContactUsPageUI;

public class ContactUsPage {
	public static boolean getemailid(WebDriver driver) {
		if (ReusableMethods.getElement(ContactUsPageUI.email, driver))
			return true;
		return false;
	}
}
