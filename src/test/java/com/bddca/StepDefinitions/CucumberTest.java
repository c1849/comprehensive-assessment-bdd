package com.bddca.StepDefinitions;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.bddca.PageObjects.ContactUsPage;
import com.bddca.PageObjects.LandingPage;
import com.bddca.PageObjects.LoginPage;
import com.bddca.PageObjects.StorePage;
import com.bddca.PageObjects.VisitUsPage;
import com.bddca.ReusableComponents.ReusableComponent;
import com.bddca.ReusableComponents.ReusableMethods;
import com.bddca.Utilities.Log;
import com.bddca.Utilities.Screenshot;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.PendingException;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CucumberTest {
	WebDriver driver;
	Logger log = Log.logger(CucumberTest.class.getName());
	
	@Before
	public void loaddriver() {
		driver = ReusableComponent.initializeDriver();
	}
	
	@Given("^User is on the Landing Page$")
	public void user_is_on_the_Landing_Page() throws Throwable {
		ReusableMethods.loadurl(driver);
		Log.logInfo(log, "Site Loaded");
	}

	@When("^User clicks on Login In$")
	public void user_clicks_on_Login_In() throws Throwable {
	    boolean result = LandingPage.clickLogIn(driver);
	    if (result)
			Log.logInfo(log, "Login click Successful");
		else
			Log.logError(log, "LogIn click Failed");
		Assert.assertTrue(result);
	}
	
	@When("User enters valid log in {string} and {string} details")
	public void user_enters_valid_log_in_and_details(String email, String password) {
		boolean result = LandingPage.enterloginDetails(driver, email, password);
	    if (result)
			Log.logInfo(log, "Login details entered Successful");
		else
			Log.logError(log, "Login details entry failed");
		Assert.assertTrue(result);
	}
	
	@Then("^User is logged in$")
	public void user_is_logged_in() throws Throwable {
		boolean result = LandingPage.profile(driver);
	    if (result)
			Log.logInfo(log, "Login Successful");
		else
			Log.logError(log, "Login Failed");
		Assert.assertTrue(result);
	}
	
	@When("User enters invalid log in {string} and {string} details")
	public void user_enters_invalid_log_in_and_details(String email, String password) {
		boolean result = LandingPage.enterloginDetails(driver, email, password);
	    if (result)
			Log.logInfo(log, "Login details entered Successful");
		else
			Log.logError(log, "Login details entry failed");
		Assert.assertTrue(result);
	}


	@Then("^Error message is diplayed$")
	public void error_message_is_diplayed() throws Throwable {
		boolean result = LoginPage.error(driver);
	    if (result)
			Log.logInfo(log, "Error diplay Successful");
		else
			Log.logError(log, "Error diplay failed");
		Assert.assertTrue(result);
	}

	@When("^User clicks on Sign Up$")
	public void user_clicks_on_Sign_Up() throws Throwable {
		boolean result = LandingPage.clickSignUp(driver);
	    if (result)
			Log.logInfo(log, "SignUp click Successful");
		else
			Log.logError(log, "SignUp click Failed");
		Assert.assertTrue(result);
	}
	
	@When("User enters valid sign up {string} and {string} details")
	public void user_enters_valid_sign_up_details(String email, String password) throws Throwable {
		boolean result = LandingPage.entersignupDetails(driver, email, password);
	    if (result)
			Log.logInfo(log, "Signup details entered Successful");
		else
			Log.logError(log, "Signup details entry failed");
		Assert.assertTrue(result);
	}
	
	@When("User enters invalid sign up {string} and {string} details")
	public void user_enters_invalid_sign_up_and_details(String email, String password) {
		boolean result = LandingPage.entersignupDetails(driver, email, password);
	    if (result)
			Log.logInfo(log, "Signup details entered Successful");
		else
			Log.logError(log, "Signup details entry failed");
		Assert.assertTrue(result);
	}
	
	@Then("^Error signup message is diplayed$")
	public void error_signup_message_is_diplayed() throws Throwable {
		boolean result = LandingPage.signupError(driver);
	    if (result)
			Log.logInfo(log, "Error diplay Successful");
		else
			Log.logError(log, "Error diplay failed");
		Assert.assertTrue(result);
	}

	@When("^User clicks Visit Us$")
	public void user_clicks_Visit_Us() throws Throwable {
		boolean result = LandingPage.clickVisitUs(driver);
	    if (result)
			Log.logInfo(log, "Visit Us click Successful");
		else
			Log.logError(log, "Visit Us click Failed");
		Assert.assertTrue(result);
	}

	@When("^User clicks on a option$")
	public void user_clicks_on_a_option() throws Throwable {
		boolean result = VisitUsPage.clickStore(driver);
	    if (result)
			Log.logInfo(log, "Store click Successful");
		else
			Log.logError(log, "Store click Failed");
		Assert.assertTrue(result);
	}

	@Then("^The address to store is displayed$")
	public void the_address_to_store_is_displayed() throws Throwable {
		boolean result = StorePage.getAddress(driver);
	    if (result)
			Log.logInfo(log, "Store Address found Successful");
		else
			Log.logError(log, "Failed to find Store address ");
		Assert.assertTrue(result);
	}

	@When("^User clicks Contact Us$")
	public void user_clicks_Contact_Us() throws Throwable {
		boolean result = LandingPage.clickContactUs(driver);
	    if (result)
			Log.logInfo(log, "ContactUs click Successful");
		else
			Log.logError(log, "ContactUs click Failed");
		Assert.assertTrue(result);
	}

	@Then("^The email address is displayed$")
	public void the_email_address_is_displayed() throws Throwable {
		boolean result = ContactUsPage.getemailid(driver);
	    if (result)
			Log.logInfo(log, "Email Address found Successful");
		else
			Log.logError(log, "Failed to find Email Address");
		Assert.assertTrue(result);
	}

	
	@After(order = 1)
	public void afterScenario(Scenario scenario) {
		if (scenario.isFailed()) {
			try {
				final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			      scenario.attach(screenshot, "image/png", "image");
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
	}

	@After(order = 0)
	public void teardown() {
		driver.quit();
	}

}
