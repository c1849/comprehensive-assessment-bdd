package com.bddca.Runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"features"}, glue = {"com.bddca.StepDefinitions"},
				 plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
				 monochrome = true,	
				 dryRun = false)
public class Runner {

}
