Feature: Authenticate

  Scenario: Login with Valid credentials
    Given User is on the Landing Page
    When User clicks on Login In
    And User enters valid log in "12@ff.fgs" and "qrqrqrqrq" details
    Then User is logged in

  Scenario Outline: Login with invalid credentials
    Given User is on the Landing Page
    When User clicks on Login In
    And User enters invalid log in "<email>" and "<password>" details
    Then Error message is diplayed

    Examples: 
      | email         | password  |
      | Joe@email.com |      1234 |
      | bob@www.com   | 87uhygfbv |

  Scenario: Sign Up with Valid Credential
    Given User is on the Landing Page
    When User clicks on Sign Up
    And User enters valid sign up "Josh@email.com" and "123qw234" details
    Then User is logged in

  Scenario Outline: Sign Up with invalid credentials
    Given User is on the Landing Page
    When User clicks on Sign Up
    And User enters invalid sign up "<email>" and "<password>" details
    Then Error signup message is diplayed

    Examples: 
      | email    | password |
      | JoeAaert |     1234 |
      | bob@www  | 12tybn78 |
