Feature: Website Info

  Scenario: Get address to the local store
    Given User is on the Landing Page
    When User clicks Visit Us
    And User clicks on a option
    Then The address to store is displayed

  Scenario: Get contact email address
    Given User is on the Landing Page
    When User clicks Contact Us
    Then The email address is displayed
